import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  readonly name: string;
  @IsNotEmpty()
  readonly pass: string;
  @IsNotEmpty()
  readonly email: string;
  readonly name_last: string;
  readonly nick: string;
  readonly image: string;
  readonly status: number;
  readonly cargo: string;
}
