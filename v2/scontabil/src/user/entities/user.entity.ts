import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  name_last: string;

  @Column()
  nick: string;

  @Column()
  email: string;

  @Column()
  pass: string;

  @Column()
  image: string;

  @Column()
  status: number;

  @Column()
  cargo: string;
}
