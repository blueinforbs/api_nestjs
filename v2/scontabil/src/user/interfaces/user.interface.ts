export interface User {
  id: number;
  name: string;
  name_last: string;
  nick: string;
  email: string;
  pass: string;
  image: string;
  status: number;
  cargo: string;
}
